1) faire un git clone du repository
2) allez dans le dossier du projet et faire composer install pour installer les dependances PHP
3) changer les valeurs du fichier .env.local pour la base de données (afin d'utiliser celles fournies par le CTI de l'IUT)
4) créer la base de données en executant php bin/console doctrine:database:create
5) lancer le serveur de developpement symfony avec symfony server:start (si vous disposez di binaire symfony) ou php bin/console server:start pour la version bundle.